module gitlab.com/gitlab-org/security-products/analyzers/gosec/v2

require (
	github.com/Microsoft/go-winio v0.6.0 // indirect
	github.com/ProtonMail/go-crypto v0.0.0-20220824120805-4b6e5c587895 // indirect
	github.com/cloudflare/circl v1.2.0 // indirect
	github.com/emirpasic/gods v1.18.1 // indirect
	github.com/google/go-cmp v0.5.8
	github.com/imdario/mergo v0.3.13 // indirect
	github.com/kevinburke/ssh_config v1.2.0 // indirect
	github.com/pelletier/go-toml v1.9.5 // indirect
	github.com/sirupsen/logrus v1.9.0
	github.com/urfave/cli/v2 v2.16.3
	github.com/xanzy/ssh-agent v0.3.2 // indirect
	gitlab.com/gitlab-org/security-products/analyzers/command v1.9.1
	gitlab.com/gitlab-org/security-products/analyzers/common/v3 v3.2.1
	gitlab.com/gitlab-org/security-products/analyzers/report/v3 v3.14.0
	gitlab.com/gitlab-org/security-products/analyzers/ruleset v1.4.0
	gitlab.com/gitlab-org/vulnerability-research/foss/vulninfo/vulninfo-go v1.4.0
	golang.org/x/crypto v0.0.0-20220926161630-eccd6366d1be
	golang.org/x/net v0.0.0-20220927171203-f486391704dc // indirect
	golang.org/x/sys v0.0.0-20220927170352-d9d178bc13c6 // indirect
)

go 1.15
